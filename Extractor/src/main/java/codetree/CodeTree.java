package codetree;

import variableprocessing.GloveVocab;
import variableprocessing.Stemming;
import variableprocessing.StopWords;
import variableprocessing.Tokenize;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.io.Serializable;

public class CodeTree implements Serializable {

    private TreeNode root;
    private int totalNumber = 0; // used to store the total number of nodes
    private Map<String, String> class_variable = new HashMap<String, String>();
    private List<String> class_variable_list = new ArrayList<>();
    private Map<String, String> class_name_map = new HashMap<>();
    private Map<String, Integer> variable_line_map = new HashMap<>();
    private Map<String, Integer> variable_use_map = new HashMap<>();
    private String functionTrace;
    private String sourceCodeTrace;
    private String sourceInfo;
    private String methodInfo;
    private boolean flag = true;

    public String getMethodInfo() {
        return methodInfo;
    }

    public void setMethodInfo(String methodInfo) {
        this.methodInfo = methodInfo;
    }

    public String getSourceInfo() {
        return sourceInfo;
    }

    public void setSourceInfo(String sourceInfo) {
        this.sourceInfo = sourceInfo;
    }

    public String getSourceCodeTrace() {
        return sourceCodeTrace;
    }

    public void setSourceCodeTrace(String sourceCodeTrace) {
        this.sourceCodeTrace = sourceCodeTrace;
    }

    public String getFunctionTrace() {
        return functionTrace;
    }

    public void setFunctionTrace(String functionTrace) {
        this.functionTrace = functionTrace;
    }

    public Map<String, Integer> getVariable_line_map() {
        return variable_line_map;
    }

    public void setVariable_line_map(Map<String, Integer> variable_line_map) {
        this.variable_line_map = variable_line_map;
    }

    public Map<String, Integer> getVariable_use_map() {
        return variable_use_map;
    }

    public void setVariable_use_map(Map<String, Integer> variable_use_map) {
        this.variable_use_map = variable_use_map;
    }

    public Map<String, String> getClass_variable() {
        return class_variable;
    }

    public void setClass_variable(Map<String, String> class_variable) {
        this.class_variable = class_variable;
    }

    public List<String> getClass_variable_list() {
        return class_variable_list;
    }

    public void setClass_variable_list(List<String> class_variable_list) {
        this.class_variable_list = class_variable_list;
    }

    public Map<String, String> getClass_name_map() {
        return class_name_map;
    }

    public void setClass_name_map(Map<String, String> class_name_map) {
        this.class_name_map = class_name_map;
    }

    public TreeNode getRoot() {
        return root;
    }

    public int getTotalNumber() {
//        if(totalNumber != 0){
//            return totalNumber;
//        }
//        else {
//            computeTotalNumberOfNodes(root);
//            return totalNumber;
//        }
        totalNumber = 0;
        computeTotalNumberOfNodes(root);
        return totalNumber;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public CodeTree() {
        root = null;
    }

    public void addNode(TreeNode node, TreeNode newNode) { // first parameter
        // "node" is the
        // node that will to
        // be the parent of
        // newNode
        if (node == null) {
            if (root == null) {
                root = newNode;
            }
        } else {
            node.getChildNodes().add(newNode);
            newNode.setParentNode(node);
        }
    }

//    public void addNodeRecurrently(TreeNode parentNode, TreeNode node) {
//        for (int i = 0; i < node.getChildNodes().size(); i++) {
//            TreeNode newNode = new TreeNode();
//            newNode.copyNode(node.getChildNodes().get(i));
//            addNode(parentNode, newNode);
//            addNodeRecurrently(newNode, node.getChildNodes().get(i));
//        }
//
//    }

    public void computeTotalNumberOfNodes(TreeNode node) {
        if (node != null) {
            totalNumber++;
            for (int i = 0; i < node.getChildNodes().size(); i++) {
                computeTotalNumberOfNodes(node.getChildNodes().get(i));
            }
        }
    }

    public CodeTree copyCodeTree(CodeTree codeTree) {
        TreeNode root = new TreeNode();
        root.copyNode(codeTree.getRoot());
        List<TreeNode> nodeList = new ArrayList<TreeNode>();
        List<TreeNode> correspondingNodeList = new ArrayList<>();
        nodeList.add(root);
        correspondingNodeList.add(codeTree.getRoot());
        while (correspondingNodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            List<TreeNode> correspondingTempList = new ArrayList<>();
            for (int i = 0; i < correspondingNodeList.size(); i++) {
                TreeNode correspondingNode = correspondingNodeList.get(i);
                TreeNode node = nodeList.get(i);
                for (int j = 0; j < correspondingNode.getChildNodes().size(); j++) {
                    TreeNode tempNode = new TreeNode();
                    tempNode.copyNode(correspondingNode.getChildNodes().get(j));
                    tempNode.setParentNode(node);
                    node.getChildNodes().add(tempNode);

                    tempList.add(tempNode);
                    correspondingTempList.add(correspondingNode.getChildNodes().get(j));
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
            correspondingNodeList.removeAll(correspondingNodeList);
            correspondingNodeList = correspondingTempList;
        }
        this.setRoot(root);
        return this;
    }

    public TreeNode getTreeNode(int serialNumber) {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(this.getRoot());
        while (nodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            for (int index = 0; index < nodeList.size(); index++) {
                TreeNode node = nodeList.get(index);
                if (node.getSerialNumber() == serialNumber) {
                    return node;
                } else {
                    for (int j = 0; j < node.getChildNodes().size(); j++) {
                        tempList.add(node.getChildNodes().get(j));
                    }
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
        }
        return null;
    }

    public TreeNode getHoleNode() {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(this.getRoot());
        while (nodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            for (int index = 0; index < nodeList.size(); index++) {
                TreeNode node = nodeList.get(index);
                if (node.getCompleteMethodDeclaration().equals("//hole")) {
                    return node;
                } else {
                    for (int j = 0; j < node.getChildNodes().size(); j++) {
                        tempList.add(node.getChildNodes().get(j));
                    }
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
        }
        return null;
    }


    public TreeNode getHoleNode2() {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(this.getRoot());
        while (nodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            for (int index = 0; index < nodeList.size(); index++) {
                TreeNode node = nodeList.get(index);
                if (node.getCompleteMethodDeclaration().equals("hole")) {
                    return node;
                } else {
                    for (int j = 0; j < node.getChildNodes().size(); j++) {
                        tempList.add(node.getChildNodes().get(j));
                    }
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
        }
        return null;
    }

    public void removeHoleNode() {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(this.getRoot());
        while (nodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            for (int index = 0; index < nodeList.size(); index++) {
                TreeNode node = nodeList.get(index);
                if (node.getCompleteMethodDeclaration().equals("hole") || node.getCompleteMethodDeclaration().equals("//hole")) {
                    node.getParentNode().getChildNodes().remove(node);
                    if(node.getChildNodes() != null && node.getChildNodes().size() == 1){
                        addNode(node.getParentNode(),node.getChildNodes().get(0));
                    }
                    node.setParentNode(null);
//                    int holeIndex = node.getParentNode().getChildNodes().indexOf(node);
//                    if(node.getChildNodes() != null && node.getChildNodes().size() == 1){
//                        node.getParentNode().getChildNodes().set(holeIndex,node.getChildNodes().get(0));
//                    }
//                    node.setParentNode(null);
                    break;
                } else {
                    for (int j = 0; j < node.getChildNodes().size(); j++) {
                        tempList.add(node.getChildNodes().get(j));
                    }
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
        }
    }

    public void dealHoleNode() {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(this.getRoot());
        while (nodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            for (int index = 0; index < nodeList.size(); index++) {
                TreeNode node = nodeList.get(index);
                if (node.getCompleteMethodDeclaration().equals("hole") || node.getCompleteMethodDeclaration().equals("//hole")) {
                    node.getParentNode().getChildNodes().remove(node);
                    if(node.getChildNodes() != null && node.getChildNodes().size() == 1){
                        addNode(node.getParentNode(),node.getChildNodes().get(0));
                    }
                    TreeNode hole = new TreeNode();
                    hole.setClassName("hole");
                    hole.setCompleteClassName("hole");
                    hole.setMethodName("");
                    hole.setCompleteMethodName("");
                    hole.setAddMethodName(false);
                    addNode(node.getParentNode(), hole);
                    node.setParentNode(null);
                    break;
                } else {
                    for (int j = 0; j < node.getChildNodes().size(); j++) {
                        tempList.add(node.getChildNodes().get(j));
                    }
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
        }
    }


    public boolean removeNode(TreeNode removeNode) {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(this.getRoot());
        while (nodeList.size() > 0) {
            List<TreeNode> tempList = new ArrayList<TreeNode>();
            for (int index = 0; index < nodeList.size(); index++) {
                TreeNode node = nodeList.get(index);
                if (node.equals(removeNode)) {
                    if (node.getParentNode() != null) {
                        if (node.getChildNodes().size() == 0) {
                            node.getParentNode().getChildNodes().remove(node);
                            return true;
                        } else if (node.getChildNodes().size() == 1) {
                            int removeIndex = node.getParentNode().getChildNodes().indexOf(node);
                            TreeNode parentNode = node.getParentNode();
                            parentNode.getChildNodes().set(removeIndex, node.getChildNodes().get(0));
                            node.getChildNodes().get(0).setParentNode(parentNode);
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        if (node.getChildNodes().size() == 0) {
                            return false;
                        } else if (node.getChildNodes().size() == 1) {
                            root = node.getChildNodes().get(0);
                            root.setParentNode(null);
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    for (int j = 0; j < node.getChildNodes().size(); j++) {
                        tempList.add(node.getChildNodes().get(j));
                    }
                }

            }
            nodeList.removeAll(nodeList);
            nodeList = tempList;
        }
        return true;//原本是return false,现在设置为true是为了过滤那些在if,while中已经被过扔掉了的结点
    }


    public void initPreviousVariables(TreeNode node, List<String> duplicatedVariableList, List<String> gloveVocabList, List<String> stopWordsList) {
        for (int i = 0; i < node.getChildNodes().size(); i++) {
            TreeNode currentNode = node.getChildNodes().get(i);
            TreeNode parentNode = currentNode.getParentNode();
            if (parentNode != null) {
                List<String> parentList = parentNode.getPreviousVariableNames();
                List<String> currentList = currentNode.getPreviousVariableNames();
                for (int j = 0; j < parentList.size(); j++) {
                    currentList.add(parentList.get(j));
                }
                String variableName = parentNode.getVariableName();
                if (variableName != null && !duplicatedVariableList.contains(variableName)) {
                    variableName = variableName.replaceAll("\r", "");
                    variableName = variableName.replaceAll("\n", "");
                    List<String> tempList = processVariables(variableName,gloveVocabList,stopWordsList);
                    for(int j = 0; j < tempList.size(); j ++){
                        if(!currentList.contains(tempList.get(j))) {
                            currentList.add(tempList.get(j));
                        }
                    }
                    duplicatedVariableList.add(variableName);
                }
            }
            initPreviousVariables(currentNode,duplicatedVariableList,gloveVocabList,stopWordsList);
        }
    }

//    public void initPreviousVariables(TreeNode node) {
//        for (int i = 0; i < node.getChildNodes().size(); i++) {
//            TreeNode currentNode = node.getChildNodes().get(i);
//            TreeNode parentNode = currentNode.getParentNode();
//            if (parentNode != null) {
//                List<String> parentList = parentNode.getPreviousVariableNames();
//                List<String> currentList = currentNode.getPreviousVariableNames();
//                for (int j = 0; j < parentList.size(); j++) {
//                    currentList.add(parentList.get(j));
//                }
//                String variableName = parentNode.getVariableName();
//                if (variableName != null) {
//                    variableName = variableName.replaceAll("\r", "");
//                    variableName = variableName.replaceAll("\n", "");
//                }
//                if (variableName != null && !currentList.contains(variableName)) {
//                    currentList.add(variableName);
//                }
//            }
//            initPreviousVariables(currentNode);
//        }
//    }

    public List<String> processVariables(String variableName, List<String> gloveVocabList, List<String> stopWordsList) {
        Tokenize tokenize = new Tokenize();
        Stemming stemming = new Stemming();
        List<String> finalList = new ArrayList<>();
        //第一步:替换掉数字及[]
        String variableWithoutNumberAndParentheses = tokenize.removeNumberAndParentheses(variableName);
        //第二步:按"_"和"$"进行split
        List<String> originalList = tokenize.splitSpecialCharacter(variableWithoutNumberAndParentheses);
        //第三步:按照Camel case进行分词(得到的词用"_"连接，还要再按照"_"进行一次split)
        List<String> camelCaseSplitList = new ArrayList<>();
        for (int j = 0; j < originalList.size(); j++) {
            String camelCaseSplitString = tokenize.splitCamelCase(originalList.get(j));
            camelCaseSplitList.add(camelCaseSplitString);
        }
        //第四步:对进行了camel case split后的单词按照"_"进行split
        List<String> camelCaseFinalList = new ArrayList<>();
        for (int j = 0; j < camelCaseSplitList.size(); j++) {
            List<String> tempList = tokenize.splitSpecialCharacter(camelCaseSplitList.get(j));
            for (int k = 0; k < tempList.size(); k++) {
                camelCaseFinalList.add(tempList.get(k));
            }
        }
        //第五步:stemming
        List<String> stemmingList = new ArrayList<>();
        for (int j = 0; j < camelCaseFinalList.size(); j++) {
            stemmingList.add(stemming.getLemma(camelCaseFinalList.get(j)));
        }
        //第六步:stopwords
        List<String> filterStopWordsList = new ArrayList<>();
        for (int j = 0; j < stemmingList.size(); j++) {
            if (!stopWordsList.contains(stemmingList.get(j)) && stemmingList.get(j).length() > 1) {
                filterStopWordsList.add(stemmingList.get(j));
            }
        }
        //第七步:Glove检查
        for (int j = 0; j < filterStopWordsList.size(); j++) {
            if (gloveVocabList.contains(filterStopWordsList.get(j))) {
                finalList.add(filterStopWordsList.get(j));
            }
        }
        return finalList;
    }

    public void processRootVariables(TreeNode node, List<String> gloveVocabList, List<String> stopWordsList) {
        List<String> variables = node.getPreviousVariableNames();
        Tokenize tokenize = new Tokenize();
        Stemming stemming = new Stemming();
        List<String> finalList = new ArrayList<>();
        for (int i = 0; i < variables.size(); i++) {
            //第一步:替换掉数字及[]
            String variableWithoutNumberAndParentheses = tokenize.removeNumberAndParentheses(variables.get(i));
            //第二步:按"_"和"$"进行split
            List<String> originalList = tokenize.splitSpecialCharacter(variableWithoutNumberAndParentheses);
            //第三步:按照Camel case进行分词(得到的词用"_"连接，还要再按照"_"进行一次split)
            List<String> camelCaseSplitList = new ArrayList<>();
            for (int j = 0; j < originalList.size(); j++) {
                String camelCaseSplitString = tokenize.splitCamelCase(originalList.get(j));
                camelCaseSplitList.add(camelCaseSplitString);
            }
            //第四步:对进行了camel case split后的单词按照"_"进行split
            List<String> camelCaseFinalList = new ArrayList<>();
            for (int j = 0; j < camelCaseSplitList.size(); j++) {
                List<String> tempList = tokenize.splitSpecialCharacter(camelCaseSplitList.get(j));
                for (int k = 0; k < tempList.size(); k++) {
                    camelCaseFinalList.add(tempList.get(k));
                }
            }
            //第五步:stemming
            List<String> stemmingList = new ArrayList<>();
            for (int j = 0; j < camelCaseFinalList.size(); j++) {
                stemmingList.add(stemming.getLemma(camelCaseFinalList.get(j)));
            }
            //第六步:stopwords
            List<String> filterStopWordsList = new ArrayList<>();
            for (int j = 0; j < stemmingList.size(); j++) {
                if (!stopWordsList.contains(stemmingList.get(j)) && stemmingList.get(j).length() > 1) {
                    filterStopWordsList.add(stemmingList.get(j));
                }
            }
            //第七步:Glove检查
            for (int j = 0; j < filterStopWordsList.size(); j++) {
                if (gloveVocabList.contains(filterStopWordsList.get(j)) && !finalList.contains(filterStopWordsList.get(j))) {
                    finalList.add(filterStopWordsList.get(j));
                }
            }
        }
        //第八步:替换Node中的previousVariableNames
        node.setPreviousVariableNames(finalList);
    }

    public void initCommentList(TreeNode node, List<String> commentList) {
        node.setCommentList(commentList);
        for (int i = 0; i < node.getChildNodes().size(); i++) {
            initCommentList(node.getChildNodes().get(i), commentList);
        }
    }

    public void filterSpecialCharacterInOriginalStatement(TreeNode node) {
        String statement = node.getStatement();
        if (statement != null) {
            statement = statement.replaceAll("\r", "");
            statement = statement.replaceAll("\n", "");
            node.setStatement(statement);
        }
        for (int i = 0; i < node.getChildNodes().size(); i++) {
            TreeNode currentNode = node.getChildNodes().get(i);
            filterSpecialCharacterInOriginalStatement(currentNode);
        }
    }

    public int getLines(TreeNode node) {
        int count = 0;
        List<TreeNode> list = node.getChildNodes();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if ("condition".equals(list.get(i).getCompleteMethodDeclaration()) ||
                        "end".equals(list.get(i).getCompleteMethodDeclaration())
                        || "break".equals(list.get(i).getCompleteMethodDeclaration())
                        || "continue".equals(list.get(i).getCompleteMethodDeclaration())
                        || "return".equals(list.get(i).getCompleteMethodDeclaration())
                        || "conditionEnd".equals(list.get(i).getCompleteMethodDeclaration())
                        || "hole".equals(list.get(i).getCompleteMethodDeclaration())
                        ) {
                    //todo nothing
                } else{
                    count ++;
                }
                count += getLines(list.get(i));
            }
            return count;
        } else {
            return count;
        }
    }


    public int getTotalPosition(TreeNode root,List<TreeNode> repeatList){
        int position = 0;
        if(!repeatList.contains(root)) {
            repeatList.add(root);
            if(!root.getCompleteMethodDeclaration().equals("conditionEnd") && !root.getCompleteMethodDeclaration().equals("end")
                    && !root.getCompleteMethodDeclaration().equals("//hole")) {
                position += 1;
            }
            for (TreeNode node : root.getChildNodes()) {
                if (!node.isCondition()) {
                    position += getTotalPosition(node,repeatList);
                }
            }
        }
        return position;
    }


    public int getPositionBeforeHole(TreeNode root,List<TreeNode> repeatList){
        int position = 0;
        if(!repeatList.contains(root) && flag) {
            repeatList.add(root);
            if(root.getCompleteMethodDeclaration().equals("//hole")){
                flag = false;
               return position;
            }else {
                if (!root.getCompleteMethodDeclaration().equals("conditionEnd") && !root.getCompleteMethodDeclaration().equals("end")
                       ) {
                    position += 1;
                }
                for (TreeNode node : root.getChildNodes()) {
                    if (!node.isCondition()) {
                        position += getPositionBeforeHole(node, repeatList);
                    }
                }
            }
        }
        return position;
    }

    public String depthFirst(TreeNode root){
        String result = "";
        if(!root.getCompleteMethodDeclaration().equals("conditionEnd")
                && !root.getCompleteMethodDeclaration().equals("end")) {
            String str = root.getCompleteMethodDeclaration();
            if(str.equals("//hole")){
                str = "/*hole*/";
            }
            str = str.replace(".", "_");
            str = str.replace(",", "_");
            str = str.replace("(", "_");
            str = str.replace(")", "_");
            str = str.replace("<", "_");
            str = str.replace(">", "_");
            str = str.replace("？", "_");
            str = str.replace("[", "_");
            str = str.replace("]", "_");
            str = str.replace("{}", "");
            result += str;
            result += "\r\n";
            for(TreeNode node:root.getChildNodes()){
                result += depthFirst(node);
            }
        }
        return result;
    }

//    public void dealHoleParentNode(TreeNode node){
//        if(node.getHoleParentNode() != null){
//            while(node.getHoleParentNode() != null){
//                if((node.getHoleParentNode().isVariableDeclaration() && node.getHoleParentNode().isPrimitive() && !node.getHoleParentNode().isVariablePreserved())
//                        || node.getHoleParentNode().isAssign()){
//                    node.setHoleParentNode(node.getHoleParentNode().getParentNode());
//                }else{
//                    break;
//                }
//            }
//            System.out.println(node.getCompleteMethodDeclaration() + "  " + node.getHoleParentNode().getCompleteMethodDeclaration());
//        }
//        List<TreeNode> list = node.getChildNodes();
//        if(list != null){
//            for(int i = 0; i < list.size(); i ++){
//                dealHoleParentNode(list.get(i));
//            }
//        }
//    }

    // 由于要考虑program
    // structure，所以一个子节点可能存在多个父节点，所以刚开始的树并不是树，而是图，但是我也用树来定义，因为只有node没有edge，我定义其为树图，得到树图后再转化为树
//    public TreeNode converterGraphToTree(TreeNode rootNode) {
//        root = new TreeNode();
//        root.copyNode(rootNode);
//        addNodeRecurrently(root, rootNode);
//        return root;
//    }

    public void processConditionHole(){
        TreeNode holeNode = this.getHoleNode();
        if(holeNode != null && holeNode.getChildNodes().size() > 0){
            TreeNode childNode = holeNode.getChildNodes().get(0);
            if("conditionEnd".equals(childNode.getCompleteMethodDeclaration())){
                holeNode.getChildNodes().remove(childNode);
                childNode.setParentNode(null);
            }
        }
    }

    public void processConditionHole(TreeNode root){
        if(root.isControl()){
            if(root.getChildNodes().size() >= 2){
                if(root.getChildNodes().get(1).getCompleteMethodDeclaration().equals("double.Constant")){
                    if(root.getChildNodes().get(1).getChildNodes().size() == 1){
                        if(root.getChildNodes().get(1).getChildNodes().get(0).getCompleteMethodDeclaration().equals("java.lang.System.out.println(double)")){
                            TreeNode tempNode = root.getChildNodes().get(1).getChildNodes().get(0);
                            if(tempNode.getChildNodes().size() == 1){
                                tempNode = tempNode.getChildNodes().get(0);
                                root.getChildNodes().remove(1);
                                root.getChildNodes().add(1,tempNode);
                                tempNode.setParentNode(root);
                            }else{
                                root.getChildNodes().remove(1);
                            }
                            TreeNode node = root.getChildNodes().get(0);
                            while(!node.getCompleteMethodDeclaration().equals("conditionEnd")){
                                node = node.getChildNodes().get(0);
                            }
                            TreeNode hole = new TreeNode();
                            hole.setCompleteMethodDeclaration("//hole");
                            node.getParentNode().getChildNodes().add(hole);
                            hole.setParentNode(node.getParentNode());
                            node.getParentNode().getChildNodes().remove(node);
                            return;
                        }
                    }
                }
            }
        }
        for(TreeNode node: root.getChildNodes()){
            processConditionHole(node);
        }
//        if(root.isControl()){
//            if(root.getChildNodes().size() >= 2){
//                if(root.getChildNodes().get(0).getCompleteMethodDeclaration().equals("conditionEnd")
//                        && root.getChildNodes().get(1).getCompleteMethodDeclaration().equals("java.lang.System.out.println(java.lang.String)")){
//                    root.getChildNodes().remove(0);
//                    TreeNode tempNode = root.getChildNodes().get(0);
//                    addNode(root,tempNode.getChildNodes().get(0));
//                    root.getChildNodes().remove(tempNode);
//                    TreeNode hole = new TreeNode();
//                    hole.setCompleteMethodDeclaration("//hole");
//                    addNode(root,hole);
//                    return;
//                }
//            }
//        }
//        for(TreeNode node: root.getChildNodes()){
//            processConditionHole(node);
//        }
    }

}
