package codetree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeNode implements Cloneable, Serializable {

    private TreeNode parentNode;
    private List<TreeNode> childNodes;
    private String className;// API simple name or control name(if, while)
    private String completeClassName; // API complete name or control name(if, while)
    private String methodName;// simple method call or control expression(if, while)
    private String completeMethodName;// complete method call or control expression(if, while)
    private List<String> argumentList;// method argument
    private List<TreeNode> dataDependency;// store data dependency
    private boolean isControl;// this flag is used to judge whether a node is a structure node(if while for)
    //	private boolean isTruePath;// used to set up the path to be true if it satisfy the if condition or loop condition
    private boolean isExit;//used to judge whether a node is the exit
    private int depth;
    private int serialNumber;
    private boolean isCondition;
    private String completeMethodDeclaration;
    private boolean isVariablePreserved = false;
    private boolean isPrimitive = false;
    private boolean isUsed = false;
    private boolean isVariableDeclaration = false;
    private boolean isAssign = false;
    private boolean isModified = true;//用来过滤同名变量的节点
    private String statement = null;//用来保存原始语句（没有进行抽象过的）
    private String variableName = null;//用来保存声明的变量名（如果当前结点是变量声明结点的话）
    private List<String> previousVariableNames = new ArrayList();//用来保存出现在当前结点前的结点
    private List<String> commentList = new ArrayList();//用来保存注释信息
    private TreeNode holeParentNode = null;
    private String info = null;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public TreeNode getHoleParentNode() {
        return holeParentNode;
    }

    public void setHoleParentNode(TreeNode holeParentNode) {
        this.holeParentNode = holeParentNode;
        //System.out.println(this.toString() + " " + holeParentNode.getCompleteMethodDeclaration());
    }

    public List<String> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<String> commentList) {
        this.commentList = commentList;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public boolean isAssign() {
        return isAssign;
    }

    public void setAssign(boolean assign) {
        isAssign = assign;
    }

    public List<String> getPreviousVariableNames() {
        return previousVariableNames;
    }

    public void setPreviousVariableNames(List<String> previousVariableNames) {
        this.previousVariableNames = previousVariableNames;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isVariablePreserved() {
        return isVariablePreserved;
    }

    public void setVariablePreserved(boolean preserved) {
        isVariablePreserved = preserved;
    }

    public boolean isVariableDeclaration() {
        return isVariableDeclaration;
    }

    public void setVariableDeclaration(boolean variableDeclaration) {
        isVariableDeclaration = variableDeclaration;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

    public void setPrimitive(boolean primitive) {
        isPrimitive = primitive;
    }

    public void setCompleteMethodDeclaration(String completeMethodDeclaration) {
        this.completeMethodDeclaration = completeMethodDeclaration;
    }

    public boolean isCondition() {
        return isCondition;
    }

    public void setCondition(boolean condition) {
        isCondition = condition;
    }

    public boolean isAddMethodName() {
        return isAddMethodName;
    }

    public void setAddMethodName(boolean addMethodName) {
        isAddMethodName = addMethodName;
    }

    private boolean isAddMethodName;

    public TreeNode() {
        completeMethodDeclaration = null;
        parentNode = null;
        childNodes = new ArrayList<TreeNode>();
        className = null;
        methodName = null;
        argumentList = new ArrayList<String>();
        dataDependency = new ArrayList<TreeNode>();
        isControl = false;
        //isTruePath = true;
        isExit = false;
        isAddMethodName = true;
        isCondition = false;
        info = null;
    }

    public List<TreeNode> getDataDependency() {
        return dataDependency;
    }

    public void setDataDependency(List<TreeNode> dataDependency) {
        this.dataDependency = dataDependency;
    }


    public TreeNode getParentNode() {
        return parentNode;
    }

    public void setParentNode(TreeNode parentNode) {
        this.parentNode = parentNode;
    }

    public List<TreeNode> getChildNodes() {
        return childNodes;
    }

    public void setChildNodes(List<TreeNode> childNodes) {
        this.childNodes = childNodes;
    }

//	public boolean isTruePath() {
//		return isTruePath;
//	}
//
//	public void setTruePath(boolean isTruePath) {
//		this.isTruePath = isTruePath;
//	}

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<String> getArgumentList() {
        return argumentList;
    }

    public void setArgumentList(List<String> argumentList) {
        this.argumentList = argumentList;
    }

    public boolean isControl() {
        return isControl;
    }

    public void setControl(boolean isControl) {
        this.isControl = isControl;
    }

    public boolean isExit() {
        return isExit;
    }

    public void setExit(boolean isExit) {
        this.isExit = isExit;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public String getCompleteMethodName() {
        return completeMethodName;
    }

    public void setCompleteMethodName(String completeMethodName) {
        this.completeMethodName = completeMethodName;
    }

    public String getCompleteClassName() {
        return completeClassName;
    }

    public void setCompleteClassName(String completeClassName) {
        this.completeClassName = completeClassName;
    }

    public String toString() {
        String str;
        if (isControl) {
            str = new String(className);
        } else if (!isAddMethodName) {
            str = new String(className);
        } else {
            str = new String(className + "." + methodName);
        }
        return str;

    }

    public String getCompleteMethodDeclaration() {
        if (completeMethodDeclaration == null) {
            if (isControl) {
                return completeClassName;
            } else if (!isAddMethodName) {
                return completeClassName;
            } else {
                return completeClassName + "." + completeMethodName;
            }
        } else {
            return completeMethodDeclaration;
        }
    }

    public Object clone() {
        Object o = null;
        try {
            o = (TreeNode) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        return o;
    }

    //
    public void copyNode(TreeNode node) {
        completeMethodDeclaration = node.getCompleteMethodDeclaration();
        parentNode = null;
        childNodes = new ArrayList<TreeNode>();
        className = node.getClassName();
        methodName = node.getMethodName();
        argumentList = node.getArgumentList();
        dataDependency = node.getDataDependency();
        isControl = node.isControl();
//		isTruePath = node.isTruePath();
        isExit = node.isExit();
        int depth = node.getDepth();
        serialNumber = node.getSerialNumber();
        completeMethodName = node.getCompleteMethodName();
        completeClassName = node.getCompleteClassName();
        isAddMethodName = node.isAddMethodName();
        isCondition = node.isCondition;
        isVariablePreserved = node.isVariablePreserved();
        isPrimitive = node.isPrimitive();
        isUsed = node.isUsed();
        isVariableDeclaration = node.isVariableDeclaration();
        isAssign = node.isAssign();
        isModified = node.isModified();
        statement = node.getStatement();
        variableName = node.getVariableName();
        previousVariableNames = node.getPreviousVariableNames();
        info = node.getInfo();
    }

    public int countNodeChildrenExcludeConditionAndEnd(TreeNode node) {
        int count = 0;
        List<TreeNode> list = node.getChildNodes();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if ("condition".equals(list.get(i).getCompleteMethodDeclaration()) ||
                        "end".equals(list.get(i).getCompleteMethodDeclaration())
                        || "break".equals(list.get(i).getCompleteMethodDeclaration())
                        || "continue".equals(list.get(i).getCompleteMethodDeclaration())
                        || "return".equals(list.get(i).getCompleteMethodDeclaration())
                        || "conditionEnd".equals(list.get(i).getCompleteMethodDeclaration())) {
                    //todo nothing
                } else {
                    count++;
                }
                count += countNodeChildrenExcludeConditionAndEnd(list.get(i));
            }
            return count;
        } else {
            return count;
        }
    }

    public boolean isContainRemoveNode(TreeNode node, TreeNode removeNode) {
        boolean result = false;
        if (node != null) {
            if (node.equals(removeNode)) {
                result = true;
                return result;
            } else {
                List<TreeNode> list = node.getChildNodes();
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).equals(removeNode)) {
                            result = true;
                            return result;
                        }
                        result = isContainRemoveNode(list.get(i), removeNode);
                        if (result) {
                            return result;
                        }
                    }
                    return result;
                } else {
                    return result;
                }
            }
        } else {
            return result;
        }
    }

}

