package constructdata;

import variableprocessing.GloveVocab;
import variableprocessing.StopWords;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by chenchi on 18/2/7.
 */
public class JudgeCodeTreeMain {
    public static void judgeCodeTreeMain(String treeSentencePath, String testCaseTracePath,String codeTreeJudgePath) throws Exception{
        long startTime = System.currentTimeMillis();
        String globalPath = System.getProperty("user.dir");
        JudgeCodeTree judgeCodeTree = new JudgeCodeTree();

        GloveVocab gloveVocab = new GloveVocab();
        List<String> gloveVocabList = gloveVocab.getGloveList();
        StopWords stopWords = new StopWords();
        List<String> stopWordsList = stopWords.getStopWordsList();
        // read jdk class name
        List<String> jdkList = new ArrayList<>();
        try {
            File fileClassNameMap = new File(globalPath + "/Extractor/src/main/java/constructdata/configs/JDKCLASS.txt");
            FileInputStream fileInputStream = new FileInputStream(fileClassNameMap);
            Scanner scanner2 = new Scanner(fileInputStream);
            while (scanner2.hasNextLine()) {
                String line = scanner2.nextLine();
                jdkList.add(line);
            }
            scanner2.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
       //FileWriter judgeWriter = createFileWriter("/Users/lingxiaoxia/Desktop/experiment/spring/codeTreeJudge.txt");
        FileWriter judgeWriter = createFileWriter(codeTreeJudgePath);
        while(true){
            if(judgeWriter != null){
                break;
            }else{
               // judgeWriter = createFileWriter("/Users/lingxiaoxia/Desktop/experiment/spring/codeTreeJudge.txt");
                judgeWriter = createFileWriter(codeTreeJudgePath);
            }
        }

        //String testCaseTrace = "/Users/lingxiaoxia/Desktop/experiment/spring/batch1/batch1.1/testCaseTrace.txt";
        String testCaseTrace = testCaseTracePath;
        File testCaseFile = new File(testCaseTrace);
        FileInputStream testCaseInputStream = new FileInputStream(testCaseFile);
        Scanner scanner = new Scanner(testCaseInputStream);
        File treeSentenceFile = new File(treeSentencePath);
        FileInputStream treeSentenceInputStream = new FileInputStream(treeSentenceFile);
        Scanner treeSentenceScanner = new Scanner(treeSentenceInputStream);
        int i = 0;
        while (scanner.hasNextLine() && treeSentenceScanner.hasNextLine()) {
            String line = scanner.nextLine();
            String treeSentence = treeSentenceScanner.nextLine();
            treeSentence = treeSentence.replaceAll(" ","");
            treeSentence = treeSentence.replaceAll("hole","");
            treeSentence = treeSentence.replace("\r\n","");
            System.out.println(++i + " " + line);
            String newTreeSentence = judgeCodeTree.judgeCodeTree(0, line, true, jdkList, false, globalPath,
                    gloveVocabList, stopWordsList);
            //System.out.println(newTreeSentence);
            if (newTreeSentence !=null && newTreeSentence.equals(treeSentence)) {
                judgeWriter.write("true" + "\r\n");
            } else {
                judgeWriter.write("false" + "\r\n");
            }

        }
        scanner.close();
        treeSentenceScanner.close();
        testCaseInputStream.close();
        treeSentenceInputStream.close();
        judgeWriter.close();
        long endTime = System.currentTimeMillis();
        String time = formatTime(endTime - startTime);
        System.out.println("total time: " + time);
    }

    public static FileWriter createFileWriter(String filePath) {
        File file = new File(filePath);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(filePath, true);
            return writer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatTime(Long ms) {
        Integer ss = 1000;
        Integer mi = ss * 60;
        Integer hh = mi * 60;
        Integer dd = hh * 24;
        Long day = ms / dd;
        Long hour = (ms - day * dd) / hh;
        Long minute = (ms - day * dd - hour * hh) / mi;
        Long second = (ms - day * dd - hour * hh - minute * mi) / ss;
        Long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;
        StringBuffer sb = new StringBuffer();
        if (day > 0) {
            sb.append(day + "d");
        }
        if (hour > 0) {
            sb.append(hour + "h");
        }
        if (minute > 0) {
            sb.append(minute + "m");
        }
        if (second > 0) {
            sb.append(second + "s");
        }
        if (milliSecond > 0) {
            sb.append(milliSecond + "ms");
        }
        return sb.toString();
    }
}
