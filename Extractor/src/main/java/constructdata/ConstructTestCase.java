package constructdata;

import codetree.CodeTree;
import codetree.CodeTreeOperation;
import codetree.TreeNode;
import predict.Predict;

import java.io.*;
import java.util.*;
public class ConstructTestCase {
    public static void main(String[] args){
        try{
            BufferedReader api_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/APIClassName.txt"));
            Map<String,Boolean> map = new HashMap<>();
            String line = null;
            while((line = api_br.readLine()) != null){
                if(!map.containsKey(line)){
                    map.put(line,true);
                }
            }
            api_br.close();

            String globalPath =  System.getProperty("user.dir");
            List<String> jdkList = new ArrayList<>();
            File fileClassNameMap = new File(globalPath + "/Extractor/src/main/java/constructdata/configs/JDKCLASS.txt");
            FileInputStream fileInputStream = new FileInputStream(fileClassNameMap);
            Scanner scanner2 = new Scanner(fileInputStream);
            while (scanner2.hasNextLine()) {
                line = scanner2.nextLine();
                jdkList.add(line);
            }
            scanner2.close();
            fileInputStream.close();

            String project = "itextTestCaseV9New";

            BufferedReader prediction_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/FSE2019GGNNTestSet/" + project + "/prediction.txt"));
            BufferedReader classPrediction_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/FSE2019GGNNTestSet/" + project + "/class.txt"));
            BufferedReader trace_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/FSE2019GGNNTestSet/" + project + "/testCaseTrace.txt"));
            BufferedReader blockPrediction_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/FSE2019GGNNTestSet/" + project + "/block_prediction.txt"));
            BufferedReader variable_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/FSE2019GGNNTestSet/" + project + "/variable_names.txt"));
            BufferedReader originalStatement_br = new BufferedReader(new FileReader("/Users/lingxiaoxia/Desktop/FSE2019GGNNTestSet/" + project + "/original_statement.txt"));

            String prediction = null;
            String classPrediction = null;
            String trace = null;
            String blockPrediction = null;
            String variable = null;
            String originalStatement = null;

            FileWriter prediction_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/trainingPrediction.txt");
            FileWriter noise_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/noise.txt");
            FileWriter classPrediction_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/trainingClassPrediction.txt");
            FileWriter blockPrediction_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/blockPredictions.txt");
            FileWriter generationNode_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/generationNode.txt");
            FileWriter holesize_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/holesize.txt");
            FileWriter lines_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/lines.txt");
            FileWriter trace_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/trace.txt");
            FileWriter trainingTree_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/trainingTree.txt");
            FileWriter trainOriginalStatements_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/trainOriginalStatements.txt");
            FileWriter trainVariableNames_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/trainVariableNames.txt");
            FileWriter treeSentence_fw = new FileWriter("/Users/lingxiaoxia/Desktop/TreeLSTMDataV9New/" + project + "/treeSentence.txt");

            int count = 0;
            while((prediction = prediction_br.readLine()) != null && (classPrediction = classPrediction_br.readLine()) != null
                    && (trace = trace_br.readLine()) != null && (blockPrediction = blockPrediction_br.readLine()) != null
                    && (variable = variable_br.readLine()) != null && (originalStatement = originalStatement_br.readLine()) != null){
                System.out.println(++count + ": " + trace);
                if(!map.containsKey(prediction)){
                   continue;
                }else {
                    Predict predict = new Predict();
                    try {
                        List<CodeTree> codeTreeList = predict.getCodeTree(trace, true, true, globalPath, jdkList, null, null);
                        CodeTree codeTree = codeTreeList.get(0);
                        if (codeTree == null) {
                            continue;
                        } else {
                            CodeTreeOperation operation = new CodeTreeOperation();
                            operation.setSerialNumberofEachNode(codeTree);
                            TreeNode holeNode = codeTree.getHoleNode();
                            if (holeNode.getParentNode() != null) {
                                int serialNumber = holeNode.getParentNode().getSerialNumber();
                                codeTree.removeHoleNode();
                                //add new hole node
                                TreeNode hole = new TreeNode();
                                hole.setClassName("hole");
                                hole.setCompleteClassName("hole");
                                hole.setMethodName("");
                                hole.setCompleteMethodName("");
                                hole.setAddMethodName(false);
                                hole.setSerialNumber(codeTree.getTotalNumber() + 1);
                                codeTree.addNode(codeTree.getTreeNode(serialNumber), hole);
                                operation.setSerialNumberofEachNode(codeTree);
                                List<Integer> list = operation.regularization(codeTree);
                                String treeNumber = new String("");
                                for (int i = 0; i < list.size(); i++) {
                                    treeNumber += list.get(i) + " ";
                                }
                                String treeSentence = operation.getTreeSentence(codeTree, true);
                                prediction_fw.write(prediction + "\r\n");
                                noise_fw.write(prediction + "\r\n");
                                classPrediction_fw.write(classPrediction + "\r\n");
                                blockPrediction_fw.write(blockPrediction + "\r\n");
                                generationNode_fw.write("0" + "\r\n");
                                holesize_fw.write("1" + "\r\n");
                                lines_fw.write("1" + "\r\n");
                                trace_fw.write(trace + "\r\n");
                                trainingTree_fw.write(treeNumber + "\r\n");
                                trainOriginalStatements_fw.write(originalStatement + "\r\n");
                                trainVariableNames_fw.write(variable + "\r\n");
                                treeSentence_fw.write(treeSentence + "\r\n");
                            }
                        }
                    }catch(Exception e){
                        continue;
                    }catch(Error e){
                        continue;
                    }
                }
            }

            prediction_br.close();
            classPrediction_br.close();
            trace_br.close();
            blockPrediction_br.close();
            variable_br.close();
            originalStatement_br.close();

            prediction_fw.close();
            noise_fw.close();
            classPrediction_fw.close();
            blockPrediction_fw.close();
            generationNode_fw.close();
            holesize_fw.close();
            lines_fw.close();
            trace_fw.close();
            trainingTree_fw.close();
            trainOriginalStatements_fw.close();
            trainVariableNames_fw.close();
            treeSentence_fw.close();

        }catch(Exception e){
            e.printStackTrace();
        }catch(Error e){
            e.printStackTrace();
        }
    }
}
