package groum.util;

/**
 * Created by zhanghr on 2018/1/30.
 */

public class Util {
    public static void print(Object[][] array){
        if (array == null)
            return;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void print(int[][] array){
        if (array == null)
            return;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
