package groum;

import japa.parser.ast.Node;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.comments.JavadocComment;
import japa.parser.ast.comments.LineComment;
import japa.parser.ast.comments.BlockComment;
import japa.parser.ast.stmt.AssertStmt;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.BreakStmt;
import japa.parser.ast.stmt.ContinueStmt;
import japa.parser.ast.stmt.DoStmt;
import japa.parser.ast.stmt.EmptyStmt;
import japa.parser.ast.stmt.ExplicitConstructorInvocationStmt;
import japa.parser.ast.stmt.ExpressionStmt;
import japa.parser.ast.stmt.ForStmt;
import japa.parser.ast.stmt.ForeachStmt;
import japa.parser.ast.stmt.IfStmt;
import japa.parser.ast.stmt.LabeledStmt;
import japa.parser.ast.stmt.ReturnStmt;
import japa.parser.ast.stmt.SwitchStmt;
import japa.parser.ast.stmt.SynchronizedStmt;
import japa.parser.ast.stmt.ThrowStmt;
import japa.parser.ast.stmt.TryStmt;
import japa.parser.ast.stmt.TypeDeclarationStmt;
import japa.parser.ast.stmt.WhileStmt;
import japa.parser.ast.visitor.VoidVisitorAdapter;

/**
 * Created by chenchi on 18/1/17.
 */
public abstract class GroumConverter extends VoidVisitorAdapter<Object>{

    protected abstract Groum convert(MethodDeclaration n);

    protected abstract Groum convert(AssertStmt n);

    protected abstract Groum convert(BlockStmt n);

    protected abstract Groum convert(BreakStmt n);

    protected abstract Groum convert(ContinueStmt n);

    protected abstract Groum convert(DoStmt n);

    protected abstract Groum convert(EmptyStmt n);

    protected abstract Groum convert(ExpressionStmt n);

    protected abstract Groum convert(ForeachStmt n);

    protected abstract Groum convert(ForStmt n);

    protected abstract Groum convert(IfStmt n);

    protected abstract Groum convert(LabeledStmt n);

    protected abstract Groum convert(ReturnStmt n);

    protected abstract Groum convert(SynchronizedStmt n);

    protected abstract Groum convert(TryStmt n);

    protected abstract Groum convert(TypeDeclarationStmt n);

    protected abstract Groum convert(WhileStmt n);

    protected abstract Groum convert(ExplicitConstructorInvocationStmt n);

    protected abstract Groum convert(SwitchStmt n);

    protected abstract Groum convert(ThrowStmt n);

    protected abstract Groum convert(LineComment n);

    protected abstract Groum convert(BlockComment n);

    protected abstract Groum convert(JavadocComment n);

    protected abstract Groum newInstance(Node n);

    @Override
    public void visit(MethodDeclaration n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(AssertStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(BlockStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(BreakStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ContinueStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(DoStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(EmptyStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ExpressionStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ForeachStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ForStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(IfStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(LabeledStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ReturnStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(SynchronizedStmt n, Object arg) {
        super.visit(n, arg);
    }

    @Override
    public void visit(TryStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(TypeDeclarationStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(WhileStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ExplicitConstructorInvocationStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(SwitchStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(ThrowStmt n, Object arg) {
        convert(n);
    }

    @Override
    public void visit(LineComment n, Object arg){convert(n);}

    @Override
    public void visit(BlockComment n, Object arg){convert(n);}

    @Override
    public void visit(JavadocComment n, Object arg){convert(n);}
//	public Groum getGroum() {
//		return result;
//	}

}
