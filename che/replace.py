import os
import sys

def find_file(file_name):
    #if file_name[-2:] != 'ts': return
    #if file_name[-4:] != 'java': return
    #if file_name[-3:] != 'xml': return
    try:
        replaced = False
        with open(file_name) as f:
            s = f.read()
            s2 = s.replace(sys.argv[1], sys.argv[2])
            replaced = s2 != s
        if replaced:
            print(file_name)
            with open(file_name, 'w') as f:
                f.write(s2)

    except Exception as e:
        pass#print(e)
    
def find_dir(dir_name):
    lst = list(os.listdir(dir_name))
    lst.sort()
    for path in lst:
        path = os.path.join(dir_name, path)

        if path.find('/target/') != -1: continue
        #if path.find('/workspace-loader/') != -1: continue

        if os.path.isdir(path):
            find_dir(path)
        elif os.path.isfile(path):
            find_file(path)

assert len(sys.argv) > 1
find_dir('.')