import os
import sys

def find_file(file_name):
    #if file_name[-2:] != 'ts': return
    #if file_name[-4:] != 'java': return
    #if file_name[-3:] != 'xml': return
    try:
        with open(file_name) as f:
            s = f.read()
            flag = True
            for arg in sys.argv[1:]:
                flag = s.find(arg) != -1
                if not flag: break
            if flag: print(file_name)
    except Exception as e:
        pass#print(e)
    
def find_dir(dir_name):
    lst = list(os.listdir(dir_name))
    lst.sort()
    for path in lst:
        path = os.path.join(dir_name, path)

        if path.find('/target/') != -1: continue
        #if path.find('/workspace-loader/') != -1: continue

        if os.path.isdir(path):
            find_dir(path)
        elif os.path.isfile(path):
            find_file(path)

assert len(sys.argv) > 1
find_dir('.')