## 项目概述:

基于上下文分析和深度学习的代码生成式补全工具DeepAPIRec是一款由复旦大学软件工程实验室CodeWisdom团队推出的基于代码上下文和深度学习的智能化API代码推荐工具。DeepAPIRec考虑代码上下文中的API使用代码及其结构信息，并通过结合Child-Sum Tree-LSTM网络和N-ary LSTM网络作为API推荐的深度学习模型。此外，DeepAPIRec构建了一个基于统计的参数模型用于实例化API中的参数并对深度学习模型推荐出的API进行重排序。 

## 特性:

1.支持单行API代码推荐。(目前支持JDK 1.8中API的推荐，以及如if, while, for等控制结构的推荐)。

2.支持补全推荐出的API中的参数。

3.支持显示推荐出的API的用法的简要文档描述。

4.支持自动导入所需要API所涉及的import信息。 

## DeepAPIRec训练数据的构造:

1. 运行Extractor/src/main/java/constructdata中的ConstructDataMain3.java。ConstructDataMain3.java中的filePaths表示后缀为.txt的所有用于训练的项目的java源文件的路径（每行一个文件路径），outputPath表示训练数据的存储路径。

2. 运行TreeLSTM/treelstm中的doOneModelV9.py脚本（其中的路径和参数根据实际情况进行修改）。

## DeepAPIRec模型的训练:

1. 运行TreeLSTM/treelstm中的TreelstmTrain.ipynb文件（其中的超参根据实际情况进行修改）。

## DeepAPIRec服务的部署:

1. 通过tomcat其中Web服务（其中的路径和参数根据实际情况进行修改）。

2. 运行TreeLSTM/treelstm中的treelstmPredictServer.py脚本（其中的参数根据实际情况进行修改）。

3. 运行TreeLSTM/py中的client.py脚本（其中的参数根据实际情况进行修改）。

注:可通过开发Eclipse，intelliJ等插件调用client.py脚本实现插件化。可参考che文件夹下的Eclipse Che插件。

## 运行环境：

python 2.7

JDK 1.8

Tensorflow 1.0.0

Tensorflow Fold 0.0.1 

